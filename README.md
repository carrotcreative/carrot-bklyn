bklyn
=====

**bklyn** a Node.js framework for creating database-driven APIs, appropriately (?) named after [a borough in New York City](http://en.wikipedia.org/wiki/Brooklyn).

### Installation

via npm:

```sh
$ npm install bklyn -g
```

### Usage

1. Create a new bklyn project:

  ```sh
  $ bklyn new MyAPI
  ```

  where `MyAPI` is the path of the folder you'd like to create.

2. Write your API:
  - Add models to the `models` directory.
  - Add controllers to the `controllers` directory.
  - Set routes in `routes.js`
  - Configure your project in `config.js`

3. To start your app:

  ```sh
  $ cd MyApp
  $ bklyn serve
  ```

### License & Contributing

- Details on the license [can be found here](LICENSE)
- Details on running tests and contributing [can be found here](contributing.md)
