var _ = require('./helpers');

var path = require('path')
  , fs = require('fsss');

module.exports = (function () {

  var Router = function (bklyn) {
    this.bklyn = bklyn;
    this.path = path.join(bklyn.path, 'routes.js');
    this.fetch = function () {
      var router = require(this.path)
        , routes = []
        , match;
      for (var key in router) {
        if (match = key.match(/^(GET|POST|PUT|PATCH|DELETE)\s(.*)$/i)) {
          routes.push(new Route(match[1], match[2], router[key]));
        }
        return routes;
      }
    }
  }

  var Route = function (method, p, fn) {
    this.method = method.toUpperCase();
    this.path = p;
    this.fn = fn;
  }

  return Router;

}());
