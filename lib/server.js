var _ = require('./helpers')
  , Err = require('./error');

var Promise = require('bluebird')
  , express = require('express');

module.exports = (function () {

  var Server = function (bklyn) {
    this.database = bklyn.database;
    this.app = bklyn.app;
    this.router = bklyn.router;
    this.config = bklyn.config;
    this.emitter = bklyn.emitter;
    this.start = function () {
      return authenticateClient.call(this)
        .bind(this)
        .then(fetchApp)
        .then(createAppFromRouter)
        .then(startServer)
        .then(emitSuccess);
    }
  }

  function authenticateClient () {
    var database = this.database
      , fetch = Promise.method(database.client.bind(database))();
    return fetch
      .then(
        function (client) {
          return client.authenticate()
            .catch(function (error) {
              throw new Err('BAD_DB_AUTHENTICATE')
            });
        }
      )
  }

  function fetchApp () {
    var fetch = this.app.fetch();
    for (var key in fetch) global[_.ucFirst(key)] = fetch[key];
    return fetch;
  }

  function createAppFromRouter (app) {
    var router = this.router
      , routes = (router.fetch() || [])
      , controllers = app.controllers
      , app = express()
      , route, fn, match
      , controller, method;
    for (var i=0; i<routes.length; i++) {
      route = routes[i];
      fn = route.fn;
      if (_.isString(fn) && (match = fn.match(/^([a-zA-Z]+)\#([a-zA-Z]+)$/))) {
        controller = controllers[match[1]];
        method = controller[match[2]];
      }
      app[route.method.toLowerCase()](route.path, method);
    }
    return app;
  }

  function startServer (app) {
    var config = this.config
      , port = (config.port || 1111);
    return new Promise(
      function (resolve, reject) {
        return app.listen(port,
          function (error) {
            !!error ? reject(error) : resolve(port);
          }
        );
      }
    );
  }

  function emitSuccess (port) {
    return this.emitter.emit('SERVER_START', port);
  }

  return Server;

}());
