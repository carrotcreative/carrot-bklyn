var Bklyn = require('./')
  , _ = require('./helpers')
  , pkg = require('../package.json');

var path = require('path')
  , colors = require('colors')
  , Promise = require('bluebird');

module.exports = (function () {

  var events = {
    SERVER_START: function (port) {
      return ('✓ started on port ' + port + '!\n').green;
    },
    PROJECT_GENERATE: function (path) {
      return ('✓ project created at ' + path + '!\n').green;
    }
  }

  var CLI = function (args) {
    this.args = args;
    var options = _.clone(args);
    delete options.path;
    delete options.action;
    this.bklyn = new Bklyn((path.resolve(process.cwd(), args.path || '')), options);
    this.emitter = this.bklyn.emitter;
    this.emitter.on('route', outputRoutes);
    if (this.bklyn.env === 'development') {
      for (var ev in events) {
        this.emitter.on(ev,
          (function (fn) {
            return function () {
              return console.log(fn.apply(this, arguments));
            }
          }(events[ev]))
        );
      }
    }
    this.run = function () {
      var action = this.args.action
        , bklyn = this.bklyn;
      var fn = (function () {
        switch (action) {
          case 'new':
            return bklyn.generate();
          case 'serve':
            return bklyn.serve();
          case 'routes':
            return Promise.method(bklyn.router.fetch.bind(bklyn.router))()
              .then(outputRoutes);
        }
      }());
      if (fn) {
        intro();
        return fn
          .catch(function (err) {
            console.error(('✖ ' + err.toString() + '.\n').red);
          });
      }
    }
  }

  function intro () {
    console.log()
    console.log(('     ' + pkg.name + '     ').inverse.bold);
    console.log((' version ' + pkg.version).grey);
    console.log();
  }

  function outputRoutes(routes) {
    var route;
    if (routes.length) {
      for (var i=0; i<routes.length; i++) {
        route = routes[i];
        console.log( ((route.method).bold + '    ' + route.path).green + '    ' + route.fn.gray )
      }
    } else {
      console.log( ('There are no routes.').grey );
    }
    console.log();
  }

  return CLI;

}());
