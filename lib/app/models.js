var Dir = require('../dir')
  , _ = require('../helpers');

var Sequelize = require('sequelize')
  , path = require('path')
  , fs = require('fsss');

module.exports = (function () {

  var associations = [
    'hasOne',
    'belongsTo',
    'belongsToMany',
    'hasMany'
  ]

  var Models = function (bklyn) {
    this.bklyn = bklyn;
    this.path = path.join(bklyn.path, 'models');
    this.database = bklyn.database;
    this.dir = new Dir(this.path, {
      t: function (type) {
        var args = Array.prototype.slice.call(arguments, 1)
          , t = Sequelize[type.toUpperCase()];
        if (args.length) return t.apply(null, args);
        return t;
      }
    });
  }

  Models.prototype = {
    fetch: function () {
      var th = this
        , requires = this.dir.fetch()
        , client = this.database.client()
        , models = {}, definitions = {}
        , name, definition, attributes
        , association;
      for (var file in requires) {
        name = _.pathStringToClassString(file, this.path);
        definitions[name] = definition = requires[file];
        if (_.isObject(definition.attributes)) {
          attributes = definition.attributes;
        }
        models[name] = client.define(name, attributes, definition.options);
      }
      for (var name in models) {
        definition = definitions[name];
        for (var i=0; i<associations.length; i++) {
          association = associations[i];
          if (_.isObject(definition[association])) {
            for (var assoc in definition[association]) {
              models[name][association](models[assoc],
                _.isObject(definition[association][assoc]) ? definition[association][assoc] : {});
            }
          }
        }
      }
      return models;
    }
  }

  return Models;

}());
