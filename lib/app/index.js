var Models = require('./models')
  , Controllers = require('./controllers');

module.exports = (function () {

  var App = function (bklyn) {
    this.models = new Models(bklyn)
    this.controllers = new Controllers(bklyn);
    this.fetch = function () {
      return {
        models: this.models.fetch(),
        controllers: this.controllers.fetch()
      }
    }
  }

  return App;

}());
