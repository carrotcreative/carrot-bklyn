var Dir = require('../dir')
  , _ = require('../helpers');

var path = require('path')
  , fs = require('fsss');

module.exports = (function () {

  var Controllers = function (bklyn) {
    this.bklyn = bklyn;
    this.path = path.join(bklyn.path, 'controllers');
    this.dir = new Dir(this.path);
  }

  Controllers.prototype = {
    fetch: function () {
      var th = this
        , requires = this.dir.fetch()
        , definitions = {}
        , definition, name;
      for (var file in requires) {
        definition = requires[file];
        name = _.pathStringToClassString(file, this.path);
        definitions[name] = definition;
      }
      return definitions;
    }
  }

  return Controllers;

}());
