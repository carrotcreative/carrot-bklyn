var Generator = require('./generator')
  , Server = require('./server')
  , App = require('./app')
  , Config = require('./config')
  , Database = require('./database')
  , Router = require('./router');

var path = require('path')
  , events = require('events');

module.exports = (function () {

  var Bklyn = function (root, options) {
    this.env = (options.env || process.env.NODE_ENV || 'development');
    this.path = path.resolve(root);
    this.config = new Config(this, options);
    this.emitter = new events.EventEmitter;
    this.database = new Database(this);
    this.app = new App(this);
    this.router = new Router(this);
    this.generate = function () {
      var generator = new Generator(this);
      return generator.exec();
    };
    this.serve = function () {
      var server = new Server(this);
      return server.start();
    };
  }

  return Bklyn;

}());
