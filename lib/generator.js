var Err = require('./error');

var path = require('path')
  , fs = require('fsss');

module.exports = (function () {

  var Generator = function (bklyn) {
    this.bklyn = bklyn;
    this.app = bklyn.app
    this.path = bklyn.path;
    this.emitter = bklyn.emitter;
    this.exec = function () {
      return createDir.call(this)
        .bind(this)
        .then(createConfig)
        .then(createModelsDir)
        .then(createControllersDir)
        .then(createRouter)
        .then(createMigrationsDir)
        .then(emitSuccess);
    };
  }

  function createDir () {
    var dir = this.path;
    return fs.mkdir(dir)
      .catch(function () {
        throw new Err('PROJECT_FOLDER_EXISTS');
      });;
  }

  function createConfig () {
    var config = this.bklyn.config
      , p = config.path;
    return fs.writeFile(p,
      'module.exports = {\n' +
      '  \n' +
      '  base: {\n' +
      '    database: {\n' +
      '      client: \'\'\n' +
      '    }\n' +
      '  },\n' +
      '  development: {\n' +
      '    database: {\n' +
      '      client: \'\'\n' +
      '    }\n' +
      '  },\n' +
      '  production: {\n' +
      '    database: {\n' +
      '      client: \'\'\n' +
      '    }\n' +
      '  }\n' +
      '  \n' +
      '}'
    );
  }

  function createModelsDir () {
    var models = this.app.models
      , dir = models.path;
    return fs.mkdir(dir)
      .then(function () {
        return createKeep(dir);
      })
  }

  function createControllersDir () {
    var controllers = this.app.controllers
      , dir = controllers.path;
    return fs.mkdir(dir)
      .then(function () {
        return createKeep(dir);
      });
  }

  function createMigrationsDir () {
    // TODO: create migrations folder
    // using path provided by client!
  }

  function createRouter () {
    var router = this.bklyn.router
      , p = router.path;
    return fs.writeFile(p,
      'module.exports = {\n' +
      '  \n' +
      '  \'GET /\': \'controller#method\'' +
      '  \n' +
      '}'
    );
  }

  function emitSuccess () {
    this.emitter.emit('PROJECT_GENERATE', this.path);
  }

  function createKeep (dir) {
    return fs.writeFile(path.join(dir, '.keep'), '');
  }

  return Generator;

}());
