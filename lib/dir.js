var _ = require('./helpers');

var path = require('path')
  , fs = require('fsss');

module.exports = (function () {

  var Dir = function (dir, locals) {
    this.path = dir;
    this.locals = locals || {};
    this.fetch = function (globals) {
      var locals = this.locals
        , previous = {}
        , output;
      for (var key in locals) {
        if ('undefined' !== typeof global[key]) {
          previous[key] = _.cloneDeep(global[key]);
        }
        global[key] = locals[key];
      }
      output = requireDir(this.path);
      for (var key in this.locals) global[key] = previous[key];
      return output;
    }
  }

  function requireDir (dir) {
    var files = fs.readdirSync(dir)
      , output = {}, file, stat, match;
    for (var i=0; i<files.length; i++) {
      file = path.join(dir, files[i]);
      stat = fs.lstatSync(file);
      if (stat.isDirectory()) {
        output = _.assign(output, requireDir(file));
      } else if (match = file.match(/^(.*)\.js$/)) {
        output[match[1]] = require(file);
      }
    }
    return output;
  }

  return Dir;

}())
