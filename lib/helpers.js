var _ = require('lodash');

_.ucFirst = function (string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

_.pathStringToClassString = function (string, base) {
  return _.ucFirst(_.camelCase(string.substr(base.length)));
}

module.exports = _;
