module.exports = (function () {

  var errors = {
    NO_DB_CONFIG: 'There is no database configuration set',
    NO_DB_CLIENT: 'You must specify a database client',
    BAD_DB_CLIENT: 'The database client specified is either missing or invalid',
    NO_DB_USER: 'You must specify a database user',
    NO_DB_PASSWORD: 'You must specify a database password',
    NO_DB_NAME: 'You must specify a database name',
    BAD_DB_AUTHENTICATE: 'The database credentials could not be authenticated',
    PROJECT_FOLDER_EXISTS: 'The project path already exists'
  }

  var Err = function (key) {
    this.key = key;
    this.toString = function () {
      return errors[key];
    }
  }

  return Err;

}());
