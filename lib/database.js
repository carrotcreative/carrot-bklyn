var Err = require('./error')

var Sequelize = require('sequelize')
  , path = require('path');

module.exports = (function () {

  var Database = function (bklyn) {
    this.path = bklyn.path;
    this.config = bklyn.config.database;
    this.client = function () {
      var config = this.config
        , options = {}
        , sequelize;
      if (config) {
        if (!config.client) throw new Err('NO_DB_CLIENT');
        options.dialectModulePath = path.join(this.path, 'node_modules', config.client);
        options.logging = false;
        try {
          require(options.dialectModulePath);
        } catch (error) {
          throw new Err('BAD_DB_CLIENT');
        }
        if (config.client) options.dialect = config.client;
        if (config.host) options.host = config.host;
        if (config.url) {
          sequelize = new Sequelize(config.url, options);
        } else {
          if (!config.name) throw new Err('NO_DB_NAME');
          if (!config.user) throw new Err('NO_DB_USER');
          if ('undefined' === typeof config.password) throw new Err('NO_DB_PASSWORD');
          sequelize = new Sequelize(config.name, config.user, config.password, options);
        }
        return sequelize;
      } else {
        throw new Err('NO_DB_CONFIG');
      }
    }
  }

  return Database;

}());
