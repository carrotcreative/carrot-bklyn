var _ = require('./helpers');

var path = require('path')
  , fs = require('fsss');

module.exports = (function () {

  var Config = function (bklyn, options) {
    this.path = path.join(bklyn.path, 'config.js');
    var env = bklyn.env
      , config = _.merge((fs.existsSync(this.path) ? require(this.path) : {}), options)
      , conf = _.merge((config.base || {}), config[env]);
    for (var key in conf) this[key] = conf[key];
  }

  return Config;

}());
